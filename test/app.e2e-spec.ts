import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'


// Let's say we have some lib that exports the class `MyError`
const AMyError = (function(){
  class MyError extends Error {}
  return MyError
})()
// and another lib exporting another class but with the same name
const BMyError = (function(){
  class MyError extends Error {}
  return MyError
})()


// ========================================================================== //
import { Catch, ExceptionFilter } from '@nestjs/common'

@Catch()
export class CatchAllFilter implements ExceptionFilter {
  catch(_exception: any, host: any) {
    const ctx = host.switchToHttp()
    const res = ctx.getResponse()
    res.send('handled by catch all')
  }
}

// The usual approach
@Catch(AMyError)
export class MyErrorFilter implements ExceptionFilter {
  catch(_exception: any, host: any) {
    const ctx = host.switchToHttp()
    const res = ctx.getResponse()
    res.send('handled by MyErrorFilter')
  }
}

// The new approach
@Catch('MyError')
export class MyErrorFilterUsingString implements ExceptionFilter {
  catch(_exception: any, host: any) {
    const ctx = host.switchToHttp()
    const res = ctx.getResponse()
    res.send('handled by MyErrorFilterUsingString')
  }
}

// Using both approaches
@Catch('MyError', AMyError)
export class MyErrorFilterUsingBoth implements ExceptionFilter {
  catch(_exception: any, host: any) {
    const ctx = host.switchToHttp()
    const res = ctx.getResponse()
    res.send('handled by MyErrorFilterUsingBoth')
  }
}


// ========================================================================== //
import { Controller, UseFilters, Get } from '@nestjs/common'

@Controller('foo')
@UseFilters(CatchAllFilter)
export class FooController {
  @Get('1')
  @UseFilters(MyErrorFilter)
  get1() {
    throw new AMyError('a')
  }

  @Get('2')
  @UseFilters(MyErrorFilterUsingString)
  get2() {
    throw new AMyError('b')
  }

  @Get('3')
  @UseFilters(MyErrorFilterUsingBoth)
  get3() {
    throw new AMyError('c')
  }
}

@Controller('bar')
@UseFilters(CatchAllFilter)
export class BarController {
  @Get('1')
  @UseFilters(MyErrorFilter)
  get1() {
    throw new BMyError('a')
  }

  @Get('2')
  @UseFilters(MyErrorFilterUsingString)
  get2() {
    throw new BMyError('b')
  }

  @Get('3')
  @UseFilters(MyErrorFilterUsingBoth)
  get3() {
    throw new BMyError('c')
  }
}


describe('AppController (E2E)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      controllers: [FooController, BarController],
    }).compile()

    app = moduleFixture.createNestApplication()

    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('FooController', () => {
    const root = 'foo'

    test.each<[path: string, status: number, body: string]>([
      ['1', 200, 'handled by MyErrorFilter'],
      ['2', 200, 'handled by MyErrorFilterUsingString'],
      ['3', 200, 'handled by MyErrorFilterUsingBoth'],
    ])
    (`GET /${root}/%s`, (path, expectedStatus, expectedBody) => {
      return request(app.getHttpServer())
        .get(`/${root}/${path}`)
        .expect(expectedStatus)
        .expect(expectedBody)
    })
  })

  describe('BarController', () => {
    const root = 'bar'

    test.each<[path: string, status: number, body: string]>([
      ['1', 200, 'handled by catch all'],
      ['2', 200, 'handled by MyErrorFilterUsingString'],
      ['3', 200, 'handled by MyErrorFilterUsingBoth'],
    ])
    (`GET /${root}/%s`, (path, expectedStatus, expectedBody) => {
      return request(app.getHttpServer())
        .get(`/${root}/${path}`)
        .expect(expectedStatus)
        .expect(expectedBody)
    })
  })
})
